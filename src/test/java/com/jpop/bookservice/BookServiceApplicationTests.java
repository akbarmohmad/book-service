package com.jpop.bookservice;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.annotation.Order;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.inject.Inject;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class BookServiceApplicationTests {

	@Inject
	private MockMvc mockMvc;

	@Test
	public void checkEmptyBooks() throws Exception {
		this.mockMvc.perform(get("/books")).andExpect(status().isOk())
				.andExpect(content().json("[]"));

	}

	@Test
	public void shouldGetAndUpdateBook() throws Exception {
		generateData();
		this.mockMvc.perform(get("/books/1")).andExpect(status().isOk())
				.andExpect(content().json("{\"bookId\":1, \"title\" : \"The Tiger\", \"author\": \"Rosy\", \"price\": 123.0, \"publishedYear\": \"2019\"}"));

		this.mockMvc.perform(put("/books/1")
				.contentType("application/json")
				.content("{\"title\" : \"The Tiger\", \"author\": \"Rosy\", \"price\": 100.0,\"publishedYear\": \"2019\"}"))
				.andExpect(status().isOk());

		cleanUp();
	}

	public void generateData() throws Exception {
		this.mockMvc.perform(post("/books")
				.contentType("application/json")
				.content("{\"title\" : \"The Tiger\", \"author\": \"Rosy\", \"price\": 123,  \"publishedYear\": \"2019\"}")).andExpect(status().isOk());
	}

	public void cleanUp() throws Exception {
		this.mockMvc.perform(delete("/books/1"));
	}

}
