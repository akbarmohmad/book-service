package com.jpop.bookservice.controller;

import com.jpop.bookservice.exception.BookNotFoundException;
import com.jpop.bookservice.model.Book;
import com.jpop.bookservice.repository.BooksRepository;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/books")
public class BooksController {

    private static final String NO_BOOK_FOUND_FOR_ID = "No Book Found for id-";

    @Inject
    private BooksRepository booksRepository;

    @GetMapping
    public List<Book> getBooks() {
        return booksRepository.findAll();
    }


    @GetMapping("/{book_id}")
    public Book getBook(@PathVariable(value = "book_id") Long bookId) {
        return booksRepository.findById(bookId).orElseThrow(() -> new BookNotFoundException("No Book Found for id-" + bookId));
    }

    @PostMapping
    public Book addBook( @Valid @RequestBody Book book) {
        return booksRepository.save(book);
    }

    @PutMapping("/{book_id}")
    public Book updateBook(@PathVariable(value = "book_id") Long bookId, @Valid @RequestBody Book bookDetails) {
        Book book = booksRepository.findById(bookId).orElseThrow(() -> new BookNotFoundException(NO_BOOK_FOUND_FOR_ID + bookId));
        book.setTitle(bookDetails.getTitle());
        book.setAuthor(bookDetails.getAuthor());
        book.setPrice(bookDetails.getPrice());
        book.setPublishedYear(bookDetails.getPublishedYear());
        booksRepository.save(book);
        return book;
    }

    @DeleteMapping("/{book_id}")
    public void deleteBook(@PathVariable(value = "book_id") Long bookId) {
        booksRepository.deleteById(bookId);
    }


}
